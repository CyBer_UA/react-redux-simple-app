import * as actionTypes from "../constants/actionTypes";

var initialState = {
    selectedIndex: -1,
    list: []
};
export default function (state = initialState, action) {

    let item;

    switch (action.type) {
        case actionTypes.REQUEST_REPOSITORIES:
            return {
                ...state,
                isFetching: action.isFetching
            };
            break;

        case actionTypes.RECEIVE_REPOSITORIES:
            const newState = {
                ...state,
                list: action.repositories,
                isFetching: action.isFetching
            };

            if (newState.selectedIndex >= 0) {
                item = newState.list[newState.selectedIndex];
                newState.list[newState.selectedIndex] = {...item, ...{
                    selected: true
                }};
            }

            return newState;
            break;

        case actionTypes.SELECT_REPOSITORY:

            if (action.repositoryIndex >= 0) {
                item = state.list[action.repositoryIndex];
                if(state.selectedIndex >= 0) {
                    state.list[state.selectedIndex] = {...state.list[state.selectedIndex], ...{
                        selected: false
                    }};
                }

                state.list[action.repositoryIndex] = {...item, ...{
                    selected: true
                }};
            }
            return {
                ...state,
                selectedIndex: action.repositoryIndex
            };
            break;
    }

    return Object.assign({}, initialState, state);
}
