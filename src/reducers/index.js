import { combineReducers } from 'redux';
import repositories from './repositories';
import select from './select';

export default combineReducers({
    repositories,
    select
});