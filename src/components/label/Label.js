import React from "react";

export default class List extends React.Component {

    render() {
        const {item} = this.props;
        return (
            <div>
                <b>name: </b> {item.name} <br/>
                <b>owner: </b>
                <img src={`${item.owner.avatar_url}.jpg`} width="50px" height="50px"/>
                <br />
                {item.owner.login}
            </div>
        )
    }
}
