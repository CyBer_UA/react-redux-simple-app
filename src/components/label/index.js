import React from "react";
import Label from "./Label";
import {connect} from "react-redux";

class LabelContainer extends React.Component {

    render() {
        const {selectedIndex, list} = this.props;
        if(list[selectedIndex]) {
            return <Label item={list[selectedIndex]} />
        }
        return null;
    }

    static get propTypes () {
        return {
            list: React.PropTypes.arrayOf(React.PropTypes.object),
            selectedIndex: React.PropTypes.number
        }
    }
}

export default connect(state => ({ //map state to property
    list: state.repositories.list || [],
    selectedIndex: state.repositories.selectedIndex
}))(LabelContainer);

