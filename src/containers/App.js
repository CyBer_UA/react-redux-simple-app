import React from "react";
import * as actions from "../actions/index";
import Label from "../components/label/index";
import Repositories from "../components/repositories/index";
import Select from "../components/select/index";
import DevTools from "./DevTools";
import {connect} from "react-redux";

class App extends React.Component {

    componentDidMount() {
        this.props.dispatch(actions.basic.fetchInitialData())
    }

    render() {
        return (
            <div>
                <div>
                    <Label />
                    <Select />
                    <Repositories />
                </div>
                <DevTools/>
            </div>
        );
    }
}

export default connect()(App);