import * as actionTypes from "../constants/actionTypes";

export function requestRepositories() {
    return {
        type: actionTypes.REQUEST_REPOSITORIES,
        isFetching: true
    }
}

export function receiveRepositories(repositories) {
    return {
        type: actionTypes.RECEIVE_REPOSITORIES,
        repositories,
        isFetching: false,
        receivedAt: Date.now()
    }
}

export function selectRepository(repositoryIndex) {
    return {
        type: actionTypes.SELECT_REPOSITORY,
        repositoryIndex
    }
}

export function fetchRepository() {
    return (dispatch, getState) => {
        dispatch(requestRepositories());
        const {select} = getState();

        fetch(`https://api.github.com/search/repositories?q=language:${select.items[select.index]}&sort=stars&order=desc`)
            .then(res => res.json())
            .then(data => {
                dispatch(receiveRepositories(data.items));
            });
    }
}

