import * as actionTypes from "../constants/actionTypes";
import {fetchRepository} from "./repositories";
import * as repositories from "./repositories";


export function languageIndexChange(selectedIndex) {
    return {
        type: actionTypes.LANGUAGE_CHANGE,
        selectedIndex
    }
}

export function requestOptions() {
    return {
        type: actionTypes.REQUEST_OPTIONS
    }
}

export function receiveOptions(items) {
    return {
        type: actionTypes.RECEIVE_OPTIONS,
        items
    }
}

export function languageChange(selectedIndex) {
    return (dispatch) => {
        dispatch(languageIndexChange(selectedIndex));
        dispatch(fetchRepository());
    }
}

export function fetchOptions() {
    return dispatch => {
        dispatch(requestOptions());
        setTimeout(() => {
            dispatch(receiveOptions(["js", "java", "c++"]));
            dispatch(repositories.fetchRepository());
        }, 200);
    }
}

